/*
 * GpioControler.cpp
 *
 *  Created on: Sep 30, 2019
 *      Author: M0nteCarl0
 */

#include "GpioControler.h"

GpioControler::GpioControler():MaxCountBank(0),MaxCountLinesPerBank(0),Iter(nullptr),Chip(nullptr){
}

GpioControler::~GpioControler(){
	MaxCountBank = 0;
	MaxCountLinesPerBank = 0;
	Iter = nullptr;
	Chip = nullptr;
}

int GpioControler::Reset(int ChipID,int Offset){
	return Write(ChipID,Offset,0);
}

int GpioControler::Init(void){
	int State = 0;
		return State;
}

int GpioControler::Set(int ChipID,int Offset){
		return Write(ChipID,Offset,1);
}

int GpioControler::Write(int ChipID,int Offset,int Value){
	char device[255];
	int State = 0;
	sprintf(device,"gpiochip%i",ChipID);
	State = gpiod_ctxless_set_value(device,Offset,Value,
			    true, "gpiowrite",
			    nullptr,
			    nullptr);
		return State;
}


int GpioControler::Read(int ChipID,int Offset)
{
	char device[255];
	int State = 0;
	sprintf(device,"gpiochip%i",ChipID);
	State = gpiod_ctxless_get_value(device, Offset,
				    true, "gpioread");
		return State;

}

int GpioControler::SetMode(int ChipID,int Offset,int Mode){
	int State = 0;
		return State;
}

const int GpioControler::Get(int ChipID,int Offset){
	int State = 0;
		return State;
}

const int GpioControler::GetMode(int ChipID,int Offset){
	int GpioMode = 0;
		return GpioMode;
}

const int GpioControler::GetChipCount(void){
	int GpioChipCount = 0;
		return GpioChipCount;

}

const int GpioControler::GetNumLines(int ChipID){
	int GpioLinesCount = 0;
		return GpioLinesCount;
}

int GpioControler::Blink(int ChipID,int Offset,int Count,int Time){
	int State = 0;
	this->Reset(ChipID,Offset);
	for(int i = 0; i < Count; i++){
		this->Set(ChipID,Offset);
		sleep(Time);
		this->Reset(ChipID,Offset);
		sleep(Time);
	}
	this->Set(ChipID,Offset);
		return State;
}
