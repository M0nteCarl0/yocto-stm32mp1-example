/*
 * GpioControler.h
 *
 *  Created on: Sep 30, 2019
 *      Author: M0nteCarl0
 */

#ifndef GPIOCONTROLER_H_
#define GPIOCONTROLER_H_

#include <unistd.h>
#include <gpiod.h>
#include <cstdio>

typedef enum GpioControlerPort
{
	GpioControlerPort_PORTA,
	GpioControlerPort_PORTB,
	GpioControlerPort_PORTC,
	GpioControlerPort_PORTD,
	GpioControlerPort_PORTE,
	GpioControlerPort_PORTF,
	GpioControlerPort_PORTG,
	GpioControlerPort_PORTH,
	GpioControlerPort_PORTI,
	GpioControlerPort_PORTJ,
}GpioControlerPort;

typedef enum GpioControler_Pin
{
	GpioControler_Pin_0,
	GpioControler_Pin_1,
	GpioControler_Pin_2,
	GpioControler_Pin_3,
	GpioControler_Pin_4,
	GpioControler_Pin_5,
	GpioControler_Pin_6,
	GpioControler_Pin_7,
	GpioControler_Pin_8,
	GpioControler_Pin_9,
	GpioControler_Pin_10,
	GpioControler_Pin_11,
	GpioControler_Pin_12,
	GpioControler_Pin_13,
	GpioControler_Pin_14,
	GpioControler_Pin_15,
}GpioControler_Pin;

class GpioControler {
public:
	GpioControler();
	virtual ~GpioControler();
	int Init(void);
    int Set(int ChipID,int Offset);
    int Reset(int ChipID,int Offset);
    int Write(int ChipID,int Offset,int Value);
    int Read(int ChipID,int Offset);
    int SetMode(int ChipID,int Offset,int Mode);
    int Blink(int ChipID,int Offset,int Count,int Time);
    const int Get(int ChipID,int Offset);
    const int GetMode(int ChipID,int Offset);
    const int GetChipCount(void);
    const int GetNumLines(int ChipID);
private:
    int MaxCountBank;
    int MaxCountLinesPerBank;
    struct gpiod_chip_iter *Iter;
    struct gpiod_chip *Chip;
};

#endif /* GPIOCONTROLER_H_ */
