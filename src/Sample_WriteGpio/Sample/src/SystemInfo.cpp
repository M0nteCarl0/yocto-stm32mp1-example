/*
 * SystemInfo.cpp
 *
 *  Created on: Sep 30, 2019
 *      Author: M0nteCarl0
 */

#include "SystemInfo.h"
#include <stdio.h>
#include <stdlib.h>

SystemInfo::SystemInfo() {
	// TODO Auto-generated constructor stub

}

SystemInfo::~SystemInfo() {
	// TODO Auto-generated destructor stub
}

void SystemInfo::ShowSocInformation(){
	SystemInfo::ShowSystemFileContent("/proc/cpuinfo");
}

 void SystemInfo::ShowLinuxInformation(void){
	 SystemInfo::ShowSystemFileContent("/etc/os-release");
 };

 void SystemInfo::ShowHostNameIformation(void){
	 SystemInfo::ShowSystemFileContent("/proc/sys/kernel/hostname");
 }

 void SystemInfo::ShowSystemFileContent(const char* PathSystemFile){
	 FILE *SystemFileinfo = fopen(PathSystemFile, "rb");
	 char *arg = 0;
	 size_t size = 0;
	 while(getdelim(&arg, &size, 0, SystemFileinfo) != -1){
		   puts(arg);
	 }
	 free(arg);
	 fclose(SystemFileinfo);
 };
