/*
 * SystemInfo.h
 *
 *  Created on: Sep 30, 2019
 *      Author: M0nteCarl0
 */

#ifndef SYSTEMINFO_H_
#define SYSTEMINFO_H_

class SystemInfo {
public:
	SystemInfo();
   ~SystemInfo();
   static void ShowSocInformation(void);
   static void ShowLinuxInformation(void);
   static void ShowHostNameIformation(void);
   static void ShowSystemFileContent(const char* PathSystemFile);
};

#endif /* SYSTEMINFO_H_ */
